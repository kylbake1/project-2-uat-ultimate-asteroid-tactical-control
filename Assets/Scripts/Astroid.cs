﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Astroid : MonoBehaviour {


   
    public GameObject player; // creates a Gameobject called player to use in inspector and script
    public Vector2 direction; // creates a 2D vector called direction

	// Use this for initialization
	void Start () {
        player = GameObject.FindWithTag("Player"); // Player is set equal to tag Player
	}

	// Update is called once per frame
	void Update()
	{
		transform.Translate(direction * Time.deltaTime * GameManager.instance.astroidSpeed); // this will move the asteroid
	}


	void OnTriggerEnter2D(Collider2D other) // if the asteroid collider is triggered
	{
		if (other.gameObject.tag == "Bullet") // By the gameobject with tag Bullet
		{

            GameManager.instance.activeEnemies.Remove(this.gameObject); // it is not active anymore
			Destroy(this.gameObject); // and deleted from game

            /* // this was for asteroid size reduction when a large asteroid is destroyed and a smaller one will respawn in it's place. needs work.
             {

            GameManager.instance.activeEnemies.Remove(this.gameObject);
			Destroy(this.gameObject);

            if (asteroidSize == 3)
            {
                // Spawn two Medium asteroids
                Instantiate(asteroidMedium, transform.position, transform.rotation);
                


            }
            else if (asteroidSize == 2)
            {
                // Spawn 2 Small asteroids
                Instantiate(asteroidSmall, transform.position, transform.rotation);


            }
            else if (asteroidSize == 1)
            {
                // Remove the asteroid and add points

            }
            Destroy(gameObject);

            // Also destroy bullet
            Destroy(other.gameObject);
		    }
            */
            
            player.SendMessage("ScorePoints", GameManager.instance.astroidScore);// Remove the asteroid and add points
           
            Destroy(other.gameObject);// Also destroy bullet
		}
	}

	void OnTriggerExit2D(Collider2D other) // If astroid collider leave the boundary
	{
		if (!GameManager.instance.removingEnemies) 
		{
			if (other.gameObject.tag == "Board") // and enters into tag Board
			{
				GameManager.instance.activeEnemies.Remove(this.gameObject); // it is set inactive 
				Destroy(this.gameObject);// and deleted from game
			}
		}
	}

}


