﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShip : MonoBehaviour {
	public Vector3 direction; // creates a 3D vector3 called direction for inspector and script
   
    public GameObject player; // creates a GameObject called player for inspector and script


    // Use this for initialization
    void Start () {
		direction = new Vector3(1, 0, 0); // direction is set equal to new vector
        player = GameObject.FindWithTag("Player"); // player is set equal to tag from player called Player for use in script and prefab
    }

    // Update is called once per frame
    void Update () {

		direction = new Vector3(1, 0, 0); 
		transform.Translate(direction * Time.deltaTime * GameManager.instance.enemyShipSpeed); // this will move the enemyship in realtime with desired speed set by user


		// Instant targeting
		direction = GameManager.instance.player.transform.position - transform.position; // direction is set in relation to players postition.
		direction.Normalize(); // this will help reduce lag or drag and make movement faster
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg; // this will rotate the enemy ship by speed rotation
		transform.rotation = Quaternion.Euler(0f, 0f, angle); // this will rotate the ship
		transform.Translate(Time.deltaTime * GameManager.instance.enemyShipSpeed, 0, 0); // this will move the ship across the board


        /*
                // Gradual targeting
                direction = GameManager.instance.player.transform.position - transform.position;
                direction.Normalize();
                float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
                Quaternion targetRotation = Quaternion.Euler(0, 0, angle);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, GameManager.instance.enemyShipRotationSpeed * Time.deltaTime);
                transform.Translate(Time.deltaTime * GameManager.instance.enemyShipSpeed, 0, 0);
        */

    }

    void OnTriggerEnter2D(Collider2D other) //  if enemyship collider is triggered
    {
        if (other.gameObject.tag == "Bullet") // by a bullet with tag Bullet
        {
            GameManager.instance.activeEnemies.Remove(this.gameObject); // this gameobject EnemyShip is removed from active list
            Destroy(this.gameObject);// and is destroyed 
           
            player.SendMessage("ScorePoints", GameManager.instance.enemyShipScore);// Remove the asteroid and add points

            Destroy(other.gameObject);// Also destroy bullet
        }
    }

    void OnTriggerExit2D(Collider2D other) // If the enemyship collider exits another collider
    {
        if (!GameManager.instance.removingEnemies) // game object is removed
        {
            if (other.gameObject.tag == "Board") // If the tag is Board 
            {
                GameManager.instance.activeEnemies.Remove(this.gameObject); // Enemyship is no longer active
                Destroy(this.gameObject); // then deleted from game hierarcy
            }
        }
    }

}
