﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	// Singleton
	public static GameManager instance; // this will allow us to use componenets from other scripts easily

	
	public GameObject[] asteroidSpawnPoints;// Astroid SpawnPoints

	
	public float astroidSpeed;// Astroid prefab

	
	public List<GameObject> enemies;// List of Astroids

	
	public GameObject bullet;// Bullet prefab
	public float bulletSpeed;// bullet speed for adjustments by user
	public float bulletLife;// time limit that bullet can live for

	
	public float enemyShipSpeed;// Enemy ship
	public float enemyShipRotationSpeed; // how fast the ship can rotate


	// List of enemies
	public List<GameObject> activeEnemies;
	public bool removingEnemies;
	public int maximumNumberActiveEnemies;

	public GameObject player; // creates gameobject called player 

	public int numLives; // creates integer called numLives to use in other scripts and inspector
    public int astroidScore; // creates integer called astroidScore to use in other scripts and inspector
    public int enemyShipScore; // creates integer called enemyShipScore to use in other scripts and inspector



	
	// Create the Singleton instance
	void Awake()
	{
		if (instance == null) // if instance is equal to nothing
		{
			instance=this; // this instance
			DontDestroyOnLoad(gameObject);// will not be destroyed
		}
		else // otherwise
		{
			Destroy(gameObject); // destroy other gameobjects
		}
	}

	// Use this for initialization
	void Start ()
	{
		activeEnemies = new List<GameObject>(); // activeEnemies is set equal to list called Gameobject
		removingEnemies = false; // bool called removeEnemies and it is equal to false. bools are always false by default
        Lives(); // lives method so player will start with 3 lives 
        
	}
	
	// Update is called once per frame
	void Update ()
	{
        
        AddEnemy(); // this will constantly add enemies automatically once one is destroyed

		if (Input.GetKeyDown(KeyCode.F)) // if user press F key
		{
			RemoveEnemies(); // all enemies are deleted
		}
        
	}

	void AddEnemy()
	{
		if (activeEnemies.Count < maximumNumberActiveEnemies) // if the number of enemies active is less than the maximum number allowed, keep adding more
		{
			// Determine spawn point
			int id = Random.Range(0, asteroidSpawnPoints.Length); // chooses from the list of spawnpoint availible in list from index 0 to 3
			GameObject point = asteroidSpawnPoints[id]; // uses a index number from list and calls it point
            
            

			// Determine which enemy to spawn
			GameObject enemy = enemies[Random.Range(0, enemies.Count)]; // this wil choose a random enemy from list of enemies 

			// Instantiate an enemy
			GameObject enemyInstance = Instantiate<GameObject>(enemy, point.transform.position, Quaternion.identity); // create a copy of enemy and set it to point which was the random spawnpoint selected

			if (enemyInstance.GetComponent<Astroid>() != null)
			{
				Vector2 directionVector = new Vector2(Random.Range(-2.0f, 2.0f), Random.Range(-2.0f, 2.0f)); // enemies will spawn in random range of spawnpoints
                transform.position = directionVector; // this will spawn them in random spots as decided above at random
                
				directionVector.Normalize(); // this will smooth out movement without drag
				enemyInstance.GetComponent<Astroid>().direction = directionVector;
			}

			// Add to enemies list
			activeEnemies.Add(enemyInstance);
		}
	}


	public void RemoveEnemies()
	{
		removingEnemies = true; // remove enemies is not set to true from false
		for (int i=0; i< activeEnemies.Count; i++)
		{
			Destroy(activeEnemies[i]); // if enemies are on field that isnt equal to 0, destroy them
		}
		activeEnemies.Clear(); // clear our activeEnemies list
		removingEnemies = false; //  revert bool back to false 
	}

    public void Lives()
    {
        numLives = 3; // numLives is set to 3 for other scripts and inspector
    }

    public void QuitGame()
    {
        Debug.Log("Quit Game"); // prints text so we can see this work before build
        Application.Quit(); // this will close down the game 

    }

   
}
