﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; // gives access to loading scenes
using UnityEngine.UI; // Using the UI System



public class Player : MonoBehaviour {
    public static GameManager instance; // this allows us to get componenets from other scripts


    Transform tf; // stores Transform in string tf;
	public float speed; // creates a public float called speed in the inspector
	public float rotationSpeed; // creates a public float called rotationSpeed in the inspector

    public GameObject gameOverPanel; // creates a GameObject called gameOverPanel to use in the inspector and other prefabs
    public SpriteRenderer spriteRenderer; // creates a SpriteRenderer holder called spriteRenderer to use in script and inspector by other prefabs
    public Collider2D collider; // creates a public Collider2D called collider 2D

    public int score; // creates public int called score in inspector and script

    public Text scoreText; // creates a public text called scoreText for the inspector and script
    public Text livesText; // creates a public text called livesText for the inspector and script



    // Use this for initialization
    void Start () // beginning void Start method
	{
		tf = GetComponent<Transform>(); // used tf for Transform and sets it equal to compnenet Transform. 
        transform.position = Vector2.zero; // transform postion of player to zero. Middle of screen.

        scoreText.text = "Score " + score; // this will display a scoreboard on UI for player
        livesText.text = "Lives " + GameManager.instance.numLives; // this will display lived for player


    } // end Start()

	// Update is called once per frame
	void Update() // beginning of void Update
	{

		if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) // player pushes W or Up and plyer moves forward
		{
			tf.transform.Translate(Time.deltaTime * speed,0,0); // this will transform player movement on screen in realtime and multiply it by desired speed in inspector
		}
		if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) // player pushes S or Down and plyer moves Backwards
        {
			tf.transform.Translate(-Time.deltaTime * speed, 0,0);// this will transform player movement on screen in realtime and multiply it by desired speed in inspector
        }
		if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) // player pushes A or Left and plyer moves Left
        {
			tf.transform.Rotate(0, 0, Time.deltaTime * rotationSpeed);// this will transform player movement on screen in realtime and multiply it by desired speed in inspector
        }
		if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) // player pushes D or Right and plyer moves Right
        {
			tf.transform.Rotate(0, 0, -Time.deltaTime * rotationSpeed);// this will transform player movement on screen in realtime and multiply it by desired speed in inspector
        }

		if (Input.GetKey(KeyCode.H))// player pushes H key
        {
			Debug.Log("Home"); // message will display in console
			tf.transform.position = new Vector3(0, 0, 0); // player teleports to middle of screen
		}

		if (Input.GetKeyDown(KeyCode.Space)) // player pushes space bar
		{
			GameObject bullet = Instantiate<GameObject>(GameManager.instance.bullet); // bullet will clone in hierarchy
			bullet.transform.position = transform.position;// bullet has movement applied to travel across screen
		}
	}

	void OnTriggerEnter2D(Collider2D other) // if player collider is entered or triggered
	{
		if (other.gameObject.tag == "Astroid") // With the Astroid collider
		{
			LifeLost(); // Player loses a life
		}
        if (other.gameObject.tag == "EnemyShip") // same goes with EnemyShip collider
        {
            LifeLost(); // Player loses another life
        }
	}

	void OnTriggerExit2D(Collider2D other) // If player leaves or triggers
	{
		if (other.gameObject.tag == "Board") // the Board boundry
		{
			LifeLost(); // Player loses a life
		}
	}

	void LifeLost()
	{
        
		GameManager.instance.numLives--; // this will reduce life count by 1
        livesText.text = "Lives " + GameManager.instance.numLives;  // will update lives for UI

        if (GameManager.instance.numLives == 0) //  if number of lives is equal to 0 using numLives from GameManager
		{
            Debug.Log("GameOver"); // Display GameOver in console
            GameOver(); // Runs GameOver Method
			//Application.Quit();

		}
		else // If player lives isnt equal to zero
		{
            GameManager.instance.RemoveEnemies(); // Gamemanager will run RemoveEnemies and clear the screen of enemies
            tf.transform.position = new Vector3(0, 0, 0); // Player is teleported back to center of screen
        }
	}

    void GameOver() 
    {
        CancelInvoke(); // This will cancel any Invokes used in any script
        spriteRenderer.enabled = false; // turns the player sprite renderer off and makes player invisible
        collider.enabled = false; // turns off the Player collider so it wont be hit
        gameOverPanel.SetActive(true); // turns the gameOverPanel on for player selection

    }

   

    public void PlayAgain() 
    {
        GameManager.instance.RemoveEnemies(); // all enemes on screen are removed and new enemies spawn
		tf.transform.position = new Vector3(0, 0, 0); // player is reset back to middle of the screen
        spriteRenderer.enabled = true; // sprite renderer is turned back on and we can see the player
        collider.enabled = true; // box collider is turned back on
        gameOverPanel.SetActive(false); // this will turn the game over panel off 
        GameManager.instance.Lives(); // this will regenerate lives for player when play again is selected
        

    }

    void ScorePoints(int pointsToAdd)
    {
        score += pointsToAdd; // += means take the score and add it and save it back to score
        scoreText.text = "Score " + score; // this will display current score
    }
}
